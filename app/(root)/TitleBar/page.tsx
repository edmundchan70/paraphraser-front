// components/TitleBar.tsx
'use client'
import React from 'react';
import { useCookies } from 'react-cookie';


const TitleBar: React.FC = () => {
    const [cookies, setCookie, removeCookie] = useCookies(['email']);
  return (
    <div className="z-10 flex items-center justify-between bg-white shadow-md py-2 px-4 text-center">
      <div className="flex items-center">
        <img src="/image.png" alt="Logo" className="w-10 h-10 mr-2" />
        <div className="text-gray-800 text-lg font-bold">Title Paraphraser</div>
      </div>
      <div className="text-gray-800 text-lg font-semibold">{cookies.email}</div>
    </div>
  );
};

export default TitleBar;
