import type { Metadata } from "next";
import { Inter } from "next/font/google";
import TitleBar from "./TitleBar/page";
import Sidebar from "./SideBar/page";
import { useCookies } from "react-cookie";


export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <div className="flex flex-col h-screen">
    <TitleBar  />
    <div className="flex flex-1">
      <Sidebar />
      <div className="flex-1 p-4">
        {children}
      </div>
    </div>
  </div>
  );
}
 