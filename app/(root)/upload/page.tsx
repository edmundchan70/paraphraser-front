"use client";
import React, { useState, useCallback, useRef } from "react";
import axios from "axios";
import { upload } from "@/app/api/uploadDocument/upload";

interface UploadedFile {
  file: File;
  name: string;
  size: number;
}

const Upload: React.FC = () => {
  const [dragging, setDragging] = useState(false);
  const [uploadedFiles, setUploadedFiles] = useState<UploadedFile[]>([]);
  const fileInputRef = useRef<HTMLInputElement | null>(null);

  const handleDragEnter = useCallback((e: React.DragEvent) => {
    e.preventDefault();
    e.stopPropagation();
    setDragging(true);
  }, []);

  const handleDragLeave = useCallback((e: React.DragEvent) => {
    e.preventDefault();
    e.stopPropagation();
    setDragging(false);
  }, []);

  const handleDragOver = useCallback((e: React.DragEvent) => {
    e.preventDefault();
    e.stopPropagation();
    setDragging(true);
  }, []);

  const handleDrop = useCallback((e: React.DragEvent) => {
    e.preventDefault();
    e.stopPropagation();
    setDragging(false);

    if (e.dataTransfer.files) {
      const files = Array.from(e.dataTransfer.files);
      const newUploadedFiles = files.map((file) => ({
        file,
        name: file.name,
        size: file.size,
      }));
      setUploadedFiles((prevFiles) => [...prevFiles, ...newUploadedFiles]);
    }
  }, []);

  const handleClick = () => {
    fileInputRef.current?.click();
  };

  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const files = Array.from(e.target.files);
      const newUploadedFiles = files.map((file) => ({
        file,
        name: file.name,
        size: file.size,
      }));
      setUploadedFiles((prevFiles) => [...prevFiles, ...newUploadedFiles]);
    }
  };

  const handleRemoveFile = (index: number) => {
    setUploadedFiles((prevFiles) => prevFiles.filter((_, i) => i !== index));
  };

  const handleSubmit = async () => {
    const formData = new FormData();
    uploadedFiles.forEach((uploadedFile) => {
      formData.append("files", uploadedFile.file);
    });
    formData.append("credential", "123@gmail.com");
    try {
      const respoonse = await upload(formData);
      console.log(respoonse);
      alert("Files uploaded successfully");
      setUploadedFiles([]);
    } catch (error) {
      console.error("Error uploading files:", error);
      alert("Failed to upload files");
    }
  };

  return (
    <div className="flex flex-col h-screen bg-gradient-to-b from-blue-50 to-blue-100">
      <div className="flex-1 flex flex-col justify-center items-center">
        <h1 className="text-3xl font-extrabold text-gray-800 mb-6">
          Upload your document for training the module
        </h1>
        <div
          onDragEnter={handleDragEnter}
          onDragLeave={handleDragLeave}
          onDragOver={handleDragOver}
          onDrop={handleDrop}
          onClick={handleClick}
          className={`w-96 h-96 border-4 border-dashed flex flex-col items-center justify-center ${dragging ? "border-blue-500 bg-blue-100" : "border-gray-300 bg-white"
            } transition-colors duration-300 cursor-pointer rounded-lg shadow-lg`}
        >
          <p className="text-gray-500">
            {dragging
              ? "Drop your file here"
              : "Drag and drop your file here or click to upload"}
          </p>
          <input
            type="file"
            ref={fileInputRef}
            onChange={handleFileChange}
            className="hidden"
            multiple
          />
        </div>
        <button
          onClick={handleSubmit}
          className="mt-4 bg-green-500 text-white px-4 py-2 rounded hover:bg-green-600 transition-colors duration-300"
        >
          Submit
        </button>
      </div>
      <div className="flex-1 p-6 bg-white overflow-auto">
        <h2 className="text-2xl font-semibold text-gray-800 mb-4">
          Uploaded Files
        </h2>
        {uploadedFiles.length === 0 ? (
          <p className="text-gray-700">No files uploaded yet</p>
        ) : (
          <ul className="space-y-3">
            {uploadedFiles.map((file, index) => (
              <li
                key={index}
                className="flex justify-between items-center p-3 bg-blue-50 rounded-lg shadow"
              >
                <div>
                  <p className="text-gray-700 font-medium">{file.name}</p>
                  <p className="text-gray-500 text-sm">
                    {(file.size / 1024).toFixed(2)} KB
                  </p>
                </div>
                <button
                  onClick={() => handleRemoveFile(index)}
                  className="bg-red-500 text-white px-3 py-1 rounded hover:bg-red-600 transition-colors duration-300"
                >
                  Remove
                </button>
              </li>
            ))}
          </ul>
        )}
      </div>
    </div>
  );
};

export default Upload;
