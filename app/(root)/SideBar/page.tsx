// components/Sidebar.tsx

import React from 'react';
import Link from 'next/link';

const Sidebar: React.FC = () => {
  return (
    <div className="h-full bg-white shadow-md flex flex-col w-1/5">
      <Link href="/upload" className="flex items-center py-4 px-6 hover:bg-gray-200 transition-colors duration-300">
        <span className="text-gray-800 text-lg">Upload your document</span>
      </Link>
      <Link href="/account" className="flex items-center py-4 px-6 hover:bg-gray-200 transition-colors duration-300">
        <span className="text-gray-800 text-lg">Account</span>
      </Link>
      <Link href="/new-writing" className="flex items-center py-4 px-6 hover:bg-gray-200 transition-colors duration-300">
        <span className="text-gray-800 text-lg">New Writing</span>
      </Link>
    </div>
  );
};

export default Sidebar;
