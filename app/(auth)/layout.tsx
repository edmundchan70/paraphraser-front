import type { Metadata } from "next";
import { Inter } from "next/font/google";


export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
     <main>
        {children}
     </main>
  );
}
