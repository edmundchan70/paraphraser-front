
import request from "../request";
import { uploadDTO } from "./DTO/upload.DTO";
 

 
// import axios from "axios";

export function upload (data: FormData) {
  return request(
    {
      url:'/userContent/upload',
      method: 'post',
      data: data
    }
  )
}