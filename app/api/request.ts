import axios from "axios";
 
 
export let expire = false;
export let url = 'localhost:8080'
 
let service = axios.create({
 
  baseURL: 'http://'+url,
 
  timeout: 30000
})

export default service;