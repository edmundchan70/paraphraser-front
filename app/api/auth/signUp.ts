// import request from '../api/request'

import request from "../request";
 
import { singUpDTO } from "./DTO/signUp.DTO";

 
// import axios from "axios";

export function signUp (data: singUpDTO) {
  return request(
    {
      url:'/auth/signUp',
      method: 'post',
      data: data
    }
  )
}