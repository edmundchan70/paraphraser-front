// import request from '../api/request'

import request from "../request";
import { singInDTO } from "./DTO/signIn.DTO";

 
// import axios from "axios";

export function signIn (data: singInDTO) {
  return request(
    {
      url:'/auth/signIn',
      method: 'post',
      data: data
    }
  )
}